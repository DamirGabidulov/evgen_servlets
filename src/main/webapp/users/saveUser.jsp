<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Save User</title>
</head>
<body>
<h1>Please enter your data:</h1>
<form action="/users" method="post" >
    <label for="userName">Enter your name</label>
    <input id="userName" type="text"  name="userName" size="20"  placeholder="Your user name" value="${user.name}">
    <input type="hidden" name="id" value="${user.id}">
    <input type="submit" value="Apply">
</form>
</body>
</html>