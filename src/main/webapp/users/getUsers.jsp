<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="models.File" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Users size - ${users.size()}</h1>
<p>${NOTIFICATION}</p>
<table>
    <tr>
        <th>Id</th>
        <th>User Name</th>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>
                <a href="?action=logIn&id=${user.id}">LogIn</a>
            </td>
        </tr>
    </c:forEach>
</table>
<p><a href="?action=save">Save new user</a></p>
</body>
</html>