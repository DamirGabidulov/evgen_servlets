<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Save File</title>
</head>
<body>
<h1>Please enter your data:</h1>
<h2><%= response.getHeader("userId") %></h2>
<form action="/files" method="post" >
    <label for="fileName">Enter your file name</label>
    <input id="fileName" type="text"  name="fileName" size="20"  placeholder="Your file name" value="${file.name}">
    <br>
    <label for="filePath">Enter your file path</label>
    <input id="filePath" type="text" name="filePath" size="20" placeholder="Your file path" value="${file.filePath}">
    <input type="hidden" name="id" value="${file.id}">

    <input type="submit" value="Apply">
</form>
<% response.setHeader("userId", response.getHeader("userId")); %>
</body>
</html>