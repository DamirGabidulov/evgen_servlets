<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="models.File" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Files size - ${files.size()}</h1>
<p>${NOTIFICATION}</p>
<table>
    <tr>
        <th>Id</th>
        <th>File Name</th>
        <th>File Path</th>
    </tr>
<c:forEach items="${files}" var="file">
    <tr>
        <td>${file.id}</td>
        <td>${file.name}</td>
        <td>${file.filePath}</td>
        <td>
            <a href="?action=edit&id=${file.id}">Edit</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="?action=delete&id=${file.id}">Delete</a>
        </td>
    </tr>


</c:forEach>

</table>
<p><a href="?action=save">Save new file</a></p>
</body>
</html>
