<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="models.File" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<table>
    <tr>
        <th>Id</th>
        <th>User Name</th>
    </tr>
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>
                <a href="?action=edit&id=${user.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="?action=delete&id=${user.id}">Delete</a>
            </td>
        </tr>
</table>
<p><a href="?action=files&id=${user.id}">My files</a></p>
<p><a href="?action=logOut&id=${user.id}">LogOut</a></p>
</body>
</html>