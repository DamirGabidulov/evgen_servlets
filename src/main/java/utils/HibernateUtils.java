package utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
    private static final String configFile = "hibernate\\hibernate.cfg.xml";

    private static SessionFactory buildSessionFactory() {
        return new Configuration().configure(configFile).buildSessionFactory();
    }

    public static Session getSession(){
        return buildSessionFactory().openSession();
    }

}
