package repositories;

import models.File;

import java.util.List;
import java.util.Optional;

public interface FileRepository extends GenericRepository<File, Integer>{

    @Override
    List<File> findAll();

    @Override
    Optional<File> getById(Integer id);

    @Override
    File save(File file);

    @Override
    File update(File file);

    @Override
    void deleteById(Integer id);
}
