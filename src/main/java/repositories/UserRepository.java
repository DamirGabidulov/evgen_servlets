package repositories;

import models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends GenericRepository<User, Integer> {
    @Override
    List<User> findAll();

    @Override
    Optional<User> getById(Integer id);

    @Override
    User save(User user);

    @Override
    User update(User user);

    @Override
    void deleteById(Integer id);
}
