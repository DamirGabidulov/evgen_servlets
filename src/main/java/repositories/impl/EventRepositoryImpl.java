package repositories.impl;

import models.Event;
import org.hibernate.Session;
import org.hibernate.query.Query;
import repositories.EventRepository;

import java.util.List;
import java.util.Optional;

import static utils.HibernateUtils.getSession;

public class EventRepositoryImpl implements EventRepository {

//    private static final String SQL_FIND_ALL = "select event from Event event left join fetch event.file";
    private static final String SQL_FIND_ALL = "from Event";
    private static final String SQL_GET_BY_ID = "select event from Event event where event.id = :id";

    @Override
    public List<Event> findAll() {
        try (Session session = getSession()){
            Query<Event> eventQuery = session.createQuery(SQL_FIND_ALL, Event.class);
            return eventQuery.getResultList();
        }
    }

    @Override
    public Optional<Event> getById(Integer id) {
        try (Session session = getSession()){
            Query<Event> eventQuery = session.createQuery(SQL_GET_BY_ID, Event.class);
            eventQuery.setParameter("id", id);
            return eventQuery.getResultList().isEmpty() ? Optional.empty() : Optional.of(eventQuery.getSingleResult());
        }
    }

    @Override
    public Event save(Event event) {
        try (Session session = getSession()){
            session.getTransaction().begin();
            session.persist(event);
            Event savedEvent = session.load(Event.class, event.getId());
            session.getTransaction().commit();
            return savedEvent;
        }
    }

    @Override
    public Event update(Event event) {
        try(Session session = getSession()){
            session.getTransaction().begin();
            session.update(event);
            Event updatedEvent = session.load(Event.class, event.getId());
            session.getTransaction().commit();
            return updatedEvent;
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = getSession()){
            session.getTransaction().begin();
            Event expectedEvent = session.load(Event.class, id);
            session.delete(expectedEvent);
            session.getTransaction().commit();
        }
    }
}
