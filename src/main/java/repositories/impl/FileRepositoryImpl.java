package repositories.impl;

import models.File;
import org.hibernate.Session;
import org.hibernate.query.Query;
import repositories.FileRepository;
import utils.HibernateUtils;

import java.util.List;
import java.util.Optional;

import static utils.HibernateUtils.getSession;

public class FileRepositoryImpl implements FileRepository {

    private static final String SQL_FIND_ALL = "from File";
    private static final String SQL_GET_BY_ID = "select file from File file where file.id = :id";

    @Override
    public List<File> findAll() {
        try (Session session = getSession()){
            Query<File> fileQuery = session.createQuery(SQL_FIND_ALL, File.class);
            return fileQuery.getResultList();
        }
    }

    @Override
    public Optional<File> getById(Integer id) {
        try (Session session = getSession()){
            Query<File> fileQuery = session.createQuery(SQL_GET_BY_ID, File.class);
            fileQuery.setParameter("id", id );
            return fileQuery.getResultList().isEmpty() ? Optional.empty() : Optional.of(fileQuery.getSingleResult());
        }
    }

    @Override
    public File save(File file) {
        try (Session session = getSession()){
        session.getTransaction().begin();
        session.persist(file);
        File savedFile = session.load(File.class, file.getId());
        session.getTransaction().commit();
        return savedFile;
        }
    }

    @Override
    public File update(File file) {
        try (Session session = getSession()){
            session.getTransaction().begin();
            session.update(file);
            File updatedFile = session.load(File.class, file.getId());
            session.getTransaction().commit();
            return updatedFile;
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = getSession()){
            session.getTransaction().begin();
            File expectedFile = session.load(File.class, id);
            session.delete(expectedFile);
            session.getTransaction().commit();
        }
    }

}
