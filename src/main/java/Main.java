import dto.FileDto;
import models.Event;
import models.File;
import models.User;
import org.w3c.dom.ls.LSInput;
import repositories.EventRepository;
import repositories.UserRepository;
import repositories.impl.EventRepositoryImpl;
import repositories.impl.UserRepositoryImpl;
import services.EventService;
import services.FileService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        FileService fileService = new FileService();
//        FileDto fileDto = FileDto.builder()
//                .name("new file")
//                .filePath("classpath:resources/")
//                .build();
//        fileService.save(fileDto);

//        System.out.println(fileService.findAll());
//        System.out.println("----");
//        System.out.println(fileService.getById(2));

        EventService eventService = new EventService();
        EventRepository eventRepository = new EventRepositoryImpl();
        UserRepository userRepository = new UserRepositoryImpl();

//        User user = User.builder()
//                .name("new")
//                .build();
//
//        UserRepository userRepository = new UserRepositoryImpl();
//        userRepository.save(user);
//
//        Event event = Event.builder()
//                .user(user)
//                .file(fileService.getById(2))
//                .build();
//
//        System.out.println(eventService.save(event));

//        System.out.println(eventService.findAll());

//        System.out.println("-----");
//
//        Event event = eventRepository.getById(6).get();
//
//        System.out.println(event);
//        System.out.println("----");
//        File file = fileService.getById(1);
//        event.setFile(file);
//
//        System.out.println(eventRepository.update(event));

//        eventRepository.deleteById(9);

        User user = userRepository.getById(3).get();
        System.out.println(user);
        System.out.println(user.getEvents().get(0).getFile());

        System.out.println("------");
        System.out.println(userRepository.findAll());

//        System.out.println("------");
//        Event eventOne = eventRepository.getById(6).get();
//        Event eventTwo = eventRepository.getById(7).get();
//        List<Event> events = new ArrayList<>();
//        events.add(eventOne);
//        events.add(eventTwo);
//        user.setEvents(events);
//        System.out.println(userRepository.update(user));
    }
}
