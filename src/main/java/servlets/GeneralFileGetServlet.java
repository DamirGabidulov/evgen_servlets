package servlets;

import models.Event;
import models.File;
import models.User;
import services.FileService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/files")
public class GeneralFileGetServlet extends HttpServlet {

    private FileService fileService;
    private UserService userService;

    @Override
    public void init() throws ServletException {
        this.fileService = new FileService();
        this.userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path = request.getParameter("action");
        System.out.println(path);

        if (path == null){
            path = "find all";
        }

        switch (path){
            case "save":
                save(request, response);
                break;
            case "edit":
                update(request, response);
                break;
            case "delete" :
                delete(request, response);
                break;
            case "find all":
            default:
                getFiles(request, response);
                break;
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        fileService.deleteById(Integer.parseInt(request.getParameter("id")));
        doNotification(request, " deleted successfully!");
        getFiles(request, response);
    }

    private void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String userId = session.getAttribute("userId").toString();
        response.addHeader("userId", userId);
        request.getRequestDispatcher("/files/generalSaveFile.jsp").forward(request, response);
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        File file = fileService.getById(Integer.parseInt(request.getParameter("id")));
        request.setAttribute("file", file);
        request.getRequestDispatcher("/files/generalSaveFile.jsp").forward(request, response);
    }

    private void getFiles(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String userId = session.getAttribute("userId").toString();
        User user = userService.getById(Integer.parseInt(userId));

        response.addHeader("userId", userId);

        List<File> files = user.getEvents().stream().map(Event::getFile).collect(Collectors.toList());
        request.setAttribute("files", files);
        request.getRequestDispatcher("/files/getFiles.jsp").forward(request, response);
    }


    /**
        fileId берется из URI, в jsp-файле есть ссылки на сохранение и обновление. edit кладет в параметр id, save нет
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileId = request.getParameter("id");
        File file = File.builder()
                .name(request.getParameter("fileName"))
                .filePath(request.getParameter("filePath"))
                .build();

        String userId = response.getHeader("userId");
        System.out.println("-----");
        System.out.println("-----");
        System.out.println(userId);
        System.out.println("-----");
        if (userId.isEmpty()){
            request.setAttribute("NOTIFICATION", "bad news");
            getFiles(request, response);
        } else {
            request.setAttribute("NOTIFICATION", userId + " caught by me");
            getFiles(request, response);
        }

        if (fileId.isEmpty()){
            fileService.save(file);
            doNotification(request, " saved successfully!");
            getFiles(request, response);
        } else {
            file.setId(Integer.parseInt(fileId));
            fileService.update(file);
            doNotification(request, " updated successfully!");
            getFiles(request, response);
        }
    }

    private void doNotification(HttpServletRequest request, String str) {
        StringBuilder notification = new StringBuilder();
        notification.append("File with id=")
                .append(request.getParameter("id"))
                .append(str);
        request.setAttribute("NOTIFICATION", notification);
    }
}

