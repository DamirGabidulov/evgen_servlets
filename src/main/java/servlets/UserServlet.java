package servlets;

import models.File;
import models.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


public class UserServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init() throws ServletException {
        this.userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getParameter("action");
        System.out.println(path);

        if (path == null) {
            path = "find all";
        }

        switch (path) {
            case "save":
                save(request, response);
                break;
            case "logIn":
                logIn(request, response);
                break;
            case "edit":
                update(request, response);
                break;
            case "delete":
                delete(request, response);
                break;
            case "logOut":
                logOut(request, response);
                break;
            case "files":
                getFilesPage(request, response);
                break;
            case "find all":
            default:
                getUsers(request, response);
                break;
        }
    }

    private void getUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> users = userService.findAll();
        request.setAttribute("users", users);
        request.getRequestDispatcher("/users/getUsers.jsp").forward(request, response);
    }

    private void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/users/saveUser.jsp").forward(request, response);
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = userService.getById(Integer.parseInt(request.getParameter("id")));

        HttpSession session = request.getSession(false);
        System.out.println(session.getAttribute("authenticated"));

        request.setAttribute("user", user);
        request.getRequestDispatcher("/users/saveUser.jsp").forward(request, response);
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        userService.deleteById(Integer.parseInt(request.getParameter("id")));
        doNotification(request, " deleted successfully!");
        HttpSession session = request.getSession(false);
        session.invalidate();
        getUsers(request, response);
    }

    private void logOut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doNotification(request, " logged out successfully!");
        HttpSession session = request.getSession(false);
        session.invalidate();
        getUsers(request, response);
    }

    private void logIn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        session.setAttribute("authenticated", true);
        String userId = request.getParameter("id");
        response.addHeader("userId", userId);
        request.getRequestDispatcher("/profile").forward(request, response);
    }

    private void getFilesPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        String userId = request.getParameter("id");
        session.setAttribute("userId", userId);
        response.sendRedirect("/files");
    }

    private void doNotification(HttpServletRequest request, String str) {
        StringBuilder notification = new StringBuilder();
        notification.append("User with id=")
                .append(request.getParameter("id"))
                .append(str);
        request.setAttribute("NOTIFICATION", notification);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        //атрибут аутентификации положили в самом начале при логине
        Boolean authenticated = (Boolean) session.getAttribute("authenticated");
        User user = User.builder()
                .name(request.getParameter("userName"))
                .build();

        if (authenticated != null && authenticated) {
            user.setId(Integer.parseInt(request.getParameter("id")));
            userService.update(user);
        } else {
            userService.save(user);
            //true - создание сессии при вызове getSession
            HttpSession newSession = request.getSession(true);
            newSession.setAttribute("authenticated", true);
        }
        //почему бы не использовать атрибут вместо хидера?
        response.addHeader("userId", user.getId().toString());
        //использую getRequestDispatcher() а не sendRedirect() так при редиректе хидер не записывается
        request.getRequestDispatcher("/profile").forward(request, response);
    }
}
