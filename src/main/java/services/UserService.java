package services;

import models.User;
import repositories.UserRepository;
import repositories.impl.UserRepositoryImpl;

import java.util.List;

public class UserService {

    private UserRepository userRepository;

    public UserService() {
        this.userRepository = new UserRepositoryImpl();
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public User getById(Integer id){
        return userRepository.getById(id).orElseThrow(() -> new IllegalArgumentException("Wrong id"));
    }

    public User update(User user){
        return userRepository.update(user);
    }

    public void deleteById(Integer id){
        userRepository.deleteById(id);
    }
}
