package services;

import models.Event;
import repositories.EventRepository;
import repositories.impl.EventRepositoryImpl;

import java.util.List;

public class EventService {

    private EventRepository eventRepository;

    public EventService() {
        this.eventRepository = new EventRepositoryImpl();
    }

    public void setEventRepository(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> findAll(){
        return eventRepository.findAll();
    }

    public Event save(Event event){
        return eventRepository.save(event);
    }
}
