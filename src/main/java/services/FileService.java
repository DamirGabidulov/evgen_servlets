package services;

import dto.FileDto;
import models.File;
import repositories.FileRepository;
import repositories.impl.FileRepositoryImpl;

import java.util.List;

public class FileService {

    private FileRepository fileRepository;

    public FileService() {
        this.fileRepository = new FileRepositoryImpl();
    }

    public void setFileRepository(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public File save(File file){
        return fileRepository.save(file);
    }

    public List<File> findAll(){
        return fileRepository.findAll();
    }

    public File getById(Integer id){
        return fileRepository.getById(id).orElseThrow(() -> new IllegalArgumentException("Wrong id"));
    }

    public File update(File file){
        return fileRepository.update(file);
    }

    public void deleteById(Integer id){
        fileRepository.deleteById(id);
    }
}
